let http = require('http');
let handler = require('./Handler');
let utils = require('./ServerUtils');
let url = require('url');
let tests = require('./ServerTests');
let dateUtils = require('./DateUtils');

let server = http.createServer(function(request, response) {
    let parts = url.parse(request.url);
    console.log(dateUtils.getDate()+" / PATH Requested: ", parts.pathname);
    if (parts.pathname.startsWith('/marketinfo/')) {
        let symbol = parts.pathname.split("/")[2];
        handler(request, response, symbol);
    } else {
        utils.sendResponse(response, "Not found", 404);
    }
});


server.listen(3000, async function () {
    console.log(dateUtils.getDate()+" / Assertion Test:");
    await tests.testing(server);
    console.log(dateUtils.getDate()+" / server start at port 3000");
});