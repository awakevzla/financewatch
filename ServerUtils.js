let https = require('https');
let dateUtils = require('./DateUtils');

exports.sendResponse = (res, data, statusCode, headers) => {
    res.writeHead(statusCode, headers);
    if(statusCode === 200){
        console.log(dateUtils.getDate()+" / Request Success!")
    }else{
        console.log(dateUtils.getDate()+" / Request Failed! Status: ",statusCode);
    }
    res.end(data);
};

exports.getData = (req, cb) => {
    let data = "";
    req.on("data", (part) => {
        data+=part
    });

    req.on("end", () => {
        cb(data)
    });
};

exports.httpRequest = (url) => {
    return new Promise(async (resolve, reject) => {
        try{
            await https.get(url, async (resp)=> {
                let data = '';
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                resp.on('end', () => {
                    let result = "";
                    try{
                        result = JSON.parse(data);
                    }catch (e) {
                        console.log(dateUtils.getDate()+" / Error parsing data");
                        console.debug(dateUtils.getDate()+" / Error info: ", e);
                    }

                    resolve(result);
                });
            });
        }catch (e) {
            return reject("")
        }
    })
};