let utils = require('./ServerUtils');
let dateUtils = require('./DateUtils');

let methods = {
    'GET': async (req, res, symbol) => {
        let data = {};
        console.log(dateUtils.getDate()+" / Symbol Consulted: ", symbol);
        let logoResponse = await utils.httpRequest("https://api.iextrading.com/1.0/stock/"+symbol+"/logo");
        let stockResponse = await utils.httpRequest("https://api.iextrading.com/1.0/stock/"+symbol+"/ohlc");
        let newsResponse = await utils.httpRequest("https://api.iextrading.com/1.0/stock/"+symbol+"/news/last/3");
        data.logo = (logoResponse.url)?logoResponse.url:"";
        data.stock_price = (stockResponse.high)?stockResponse.high:"";
        data.news = (newsResponse)?newsResponse:"";
        if (data.logo === "" || data.stock_price === "" || data.news === "") {
            utils.sendResponse(res, "Symbol not found", 404, {'Content-Type': 'text/plain'})
        } else {
            utils.sendResponse(res, JSON.stringify(data), 200, {'Content-Type': 'application/json'});
        }
    }
};

module.exports = (req, res, symbol) => {
    let method = methods[req.method];
    if (method) {
        method(req, res, symbol);
    } else {
        utils.sendResponse(res, "Not Found", 404);
    }
};