#Ideaware Barranquilla Test


## For Local Deployment

- Clone this repository
- Install npm & NodeJS
- run `npm run start`
    - Automatically run the test, if the asserts fail, the server close.
- And it's done!

## For Testing

- Run the follow curl to test the market information:

    ```
    curl -X GET \
      http://localhost:3000/marketinfo/aapl
    ```
    
    
###Created by Pedro Lugo <robertty55@gmail.com>