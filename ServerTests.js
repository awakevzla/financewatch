let assert = require('assert');
let http = require('http');
let dateUtils = require('./DateUtils');

exports.testing = async (server) => {

    await http.get("http://localhost:3000/marketinfo/aapl", (resp) => {
        console.log(dateUtils.getDate()+" / STATUS:"+resp.statusCode);
        try{
            assert.strictEqual(resp.statusCode, 200, "Status code distinct to 200");
            console.log(dateUtils.getDate()+" / Status Code 200 PASS");
        }catch (e) {
            console.log(dateUtils.getDate()+" / Error: ", e);
            console.log(dateUtils.getDate()+" / Assert fail!");
            server.close();
        }
    });

    await http.get("http://localhost:3000/marketinfo/NOT-EXIST", (resp) => {
        console.log(dateUtils.getDate()+" / STATUS:"+resp.statusCode);
        try{
            assert.strictEqual(resp.statusCode, 404, "Status code distinct to 404");
            console.log(dateUtils.getDate()+" / Status Code 404 PASS");

        }catch (e) {
            console.log(dateUtils.getDate()+" / Error: ", e);
            console.log(dateUtils.getDate()+" / Assert fail!");
            server.close();
        }
    });
}